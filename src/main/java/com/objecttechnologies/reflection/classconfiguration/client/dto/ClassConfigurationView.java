package com.objecttechnologies.reflection.classconfiguration.client.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ClassConfigurationView {
    String className;
    Boolean active;
}
