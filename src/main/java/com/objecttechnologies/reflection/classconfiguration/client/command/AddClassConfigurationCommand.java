package com.objecttechnologies.reflection.classconfiguration.client.command;

import lombok.Data;

@Data
public class AddClassConfigurationCommand {
    String className;
}
