package com.objecttechnologies.reflection.classconfiguration.client.command;

import lombok.Value;

@Value
public class ChangeClassConfigurationCommand {
    String className;
    Boolean active;
}
