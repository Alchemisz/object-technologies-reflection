package com.objecttechnologies.reflection.classconfiguration.client;

import com.objecttechnologies.reflection.classconfiguration.client.command.AddClassConfigurationCommand;
import com.objecttechnologies.reflection.classconfiguration.client.command.ChangeClassConfigurationCommand;
import com.objecttechnologies.reflection.classconfiguration.client.dto.ClassConfigurationView;
import com.objecttechnologies.reflection.classconfiguration.infrastructure.ClassConfigurationCommandPort;
import com.objecttechnologies.reflection.classconfiguration.infrastructure.ClassConfigurationRepositoryPort;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/class-configurations")
@RequiredArgsConstructor
public class ClassConfigurationController {

    private final ClassConfigurationCommandPort classConfigurationCommandPort;
    private final ClassConfigurationRepositoryPort classConfigurationRepositoryPort;

    @GetMapping
    public List<ClassConfigurationView> getAll() {
        return classConfigurationRepositoryPort.findAll();
    }

    @PatchMapping("/change")
    public void changeClassConfiguration(@RequestBody ChangeClassConfigurationCommand command) {
        classConfigurationCommandPort.changeConfiguration(command);
    }

    @PostMapping("/add")
    public void addClassConfiguration(@RequestBody AddClassConfigurationCommand command) {
        classConfigurationCommandPort.addConfiguration(command);
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException(Exception exception) {
        return exception.getMessage();
    }

}
