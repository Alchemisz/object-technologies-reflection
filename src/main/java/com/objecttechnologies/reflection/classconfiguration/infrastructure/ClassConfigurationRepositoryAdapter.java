package com.objecttechnologies.reflection.classconfiguration.infrastructure;

import com.objecttechnologies.reflection.classconfiguration.client.dto.ClassConfigurationView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class ClassConfigurationRepositoryAdapter implements ClassConfigurationRepositoryPort {

    private final ClassConfigurationRepository classConfigurationRepository;

    @Override
    public List<ClassConfigurationView> findAll() {
        return classConfigurationRepository.findAll().stream()
            .map(classConfiguration ->
                ClassConfigurationView.builder()
                    .className(classConfiguration.getClassName())
                    .active(classConfiguration.getActive())
                    .build()
            )
            .collect(Collectors.toList());
    }

    @Override
    public Set<String> findActiveClassName() {
        return classConfigurationRepository.findAll().stream()
            .filter(ClassConfigurationEntity::getActive)
            .map(ClassConfigurationEntity::getClassName)
            .collect(Collectors.toSet());
    }

    @Override
    public ClassConfigurationEntity findById(String className) {
        return classConfigurationRepository.findById(className)
            .orElseThrow(() -> new IllegalStateException(String.format("Can not find configuration for class: %s", className)));
    }

    @Override
    public void update(ClassConfigurationEntity classConfiguration) {
        classConfigurationRepository.update(classConfiguration);
    }

    @Override
    public void persist(ClassConfigurationEntity classConfiguration) {
        classConfigurationRepository.persist(classConfiguration);
    }
}