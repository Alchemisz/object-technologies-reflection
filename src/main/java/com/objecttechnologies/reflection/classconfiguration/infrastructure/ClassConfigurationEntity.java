package com.objecttechnologies.reflection.classconfiguration.infrastructure;

import com.objecttechnologies.reflection.classconfiguration.client.command.AddClassConfigurationCommand;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(staticName = "of")
class ClassConfigurationEntity {
    private final String className;
    private Boolean active;

    static ClassConfigurationEntity create(AddClassConfigurationCommand command) {
        return ClassConfigurationEntity.of(command.getClassName(), true);
    }

    void activate() {
        this.active = true;
    }

    void deactivate() {
        this.active = false;
    }
}
