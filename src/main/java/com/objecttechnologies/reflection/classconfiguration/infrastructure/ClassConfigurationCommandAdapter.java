package com.objecttechnologies.reflection.classconfiguration.infrastructure;

import com.objecttechnologies.reflection.classconfiguration.client.command.AddClassConfigurationCommand;
import com.objecttechnologies.reflection.classconfiguration.client.command.ChangeClassConfigurationCommand;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class ClassConfigurationCommandAdapter implements ClassConfigurationCommandPort {

    private final ClassConfigurationRepositoryPort classConfigurationRepositoryPort;

    @Override
    public void changeConfiguration(ChangeClassConfigurationCommand command) {
        if (command.getActive() != null) {
            handleUpdate(command);
        }
    }

    @Override
    public void addConfiguration(AddClassConfigurationCommand command) {
        try {
            Class.forName(command.getClassName());
            ClassConfigurationEntity classConfigurationEntity = ClassConfigurationEntity.create(command);
            classConfigurationRepositoryPort.persist(classConfigurationEntity);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(String.format("Can't find class with path: %s", command.getClassName()));
        }
    }

    private void handleUpdate(ChangeClassConfigurationCommand command) {
        var classConfigurationEntity = classConfigurationRepositoryPort.findById(command.getClassName());
        if (Boolean.TRUE.equals(command.getActive())) {
            classConfigurationEntity.activate();
        } else {
            classConfigurationEntity.deactivate();
        }
        classConfigurationRepositoryPort.update(classConfigurationEntity);
    }
}
