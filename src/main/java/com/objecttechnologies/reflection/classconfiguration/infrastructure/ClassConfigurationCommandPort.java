package com.objecttechnologies.reflection.classconfiguration.infrastructure;

import com.objecttechnologies.reflection.classconfiguration.client.command.AddClassConfigurationCommand;
import com.objecttechnologies.reflection.classconfiguration.client.command.ChangeClassConfigurationCommand;

public interface ClassConfigurationCommandPort {

    void changeConfiguration(ChangeClassConfigurationCommand command);

    void addConfiguration(AddClassConfigurationCommand command);
}
