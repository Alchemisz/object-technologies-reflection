package com.objecttechnologies.reflection.classconfiguration.infrastructure;


import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class ClassConfigurationRepository {

    private final Map<String, ClassConfigurationEntity> configurationByClassName;

    public ClassConfigurationRepository(ClassNameScannerService classNameScannerService) {
        this.configurationByClassName = classNameScannerService.findAll().stream()
            .collect(
                Collectors.toMap(
                    className -> className,
                    className -> ClassConfigurationEntity.of(className, true)
                )
            );
    }

    public Collection<ClassConfigurationEntity> findAll() {
        return configurationByClassName.values();
    }

    public void update(ClassConfigurationEntity classConfiguration) {
        configurationByClassName.put(classConfiguration.getClassName(), classConfiguration);
    }

    public void persist(ClassConfigurationEntity classConfiguration) {
        configurationByClassName.put(classConfiguration.getClassName(), classConfiguration);
    }

    public Optional<ClassConfigurationEntity> findById(String className) {
        return Optional.of(configurationByClassName.get(className));
    }
}
