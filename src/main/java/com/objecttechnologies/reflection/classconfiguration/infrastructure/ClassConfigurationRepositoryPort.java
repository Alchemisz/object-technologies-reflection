package com.objecttechnologies.reflection.classconfiguration.infrastructure;

import com.objecttechnologies.reflection.classconfiguration.client.dto.ClassConfigurationView;

import java.util.List;
import java.util.Set;

public interface ClassConfigurationRepositoryPort {
    List<ClassConfigurationView> findAll();

    Set<String> findActiveClassName();

    ClassConfigurationEntity findById(String className);

    void update(ClassConfigurationEntity classConfiguration);

    void persist(ClassConfigurationEntity classConfiguration);
}
