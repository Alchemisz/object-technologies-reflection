package com.objecttechnologies.reflection.classconfiguration.infrastructure;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class ClassNameScannerService {

    private static final String STORAGE_PACKAGE = "com.objecttechnologies.reflection.classes.storage";

    public Set<String> findAll() {
        InputStream stream = ClassLoader.getSystemClassLoader()
            .getResourceAsStream(STORAGE_PACKAGE.replaceAll("[.]", "/"));
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        return reader.lines()
            .filter(line -> line.endsWith(".class"))
            .map(line -> getClass(line).getName())
            .collect(Collectors.toSet());
    }

    private Class<?> getClass(String className) {
        try {
            return Class.forName(STORAGE_PACKAGE + "."
                + className.substring(0, className.lastIndexOf('.')));
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

}
