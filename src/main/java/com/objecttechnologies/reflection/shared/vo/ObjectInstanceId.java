package com.objecttechnologies.reflection.shared.vo;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Value
@ToString
@RequiredArgsConstructor(staticName = "of")
public class ObjectInstanceId {
    String objectInstanceId;

    public String stringValue() {
        return objectInstanceId;
    }
}
