package com.objecttechnologies.reflection.shared.utils;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AppUtils {

    public static boolean isEmpty(Object[] objects) {
        return objects == null || objects.length == 0;
    }

}
