package com.objecttechnologies.reflection.classes.storage;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public class Student {

    private static final String SECRET = "SECRET";

    private String name;
    private Integer age;
    private Address address;

    private Student() {
    }

    public Student(String name) {
        this.name = name;
    }

    public Student(Student student) {
        this.age = student.age;
        this.name = student.name;
    }

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    void increaseAge() {
        this.age++;
    }

    void decreaseAge() {
        this.age--;
    }

    private void print() {
        System.out.println(this);
    }
}
