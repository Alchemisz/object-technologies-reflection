package com.objecttechnologies.reflection.classes.storage;

import lombok.ToString;

@ToString
class Ghost {
    String name;
    Boolean visible;

    public Ghost(String name, Boolean visible) {
        this.name = name;
        this.visible = visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }
}
