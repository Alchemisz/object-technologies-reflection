package com.objecttechnologies.reflection.classes.storage;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private String name;
    private List<Student> students;

    private Group() {
    }

    public Group(String name) {
        this.name = name;
    }

    public Group(String name, List<Student> students) {
        this.name = name;
        this.students = students;
    }

    public void addStudent(Student student) {
        if (this.students == null) {
            this.students = new ArrayList<>();
        }
        this.students.add(student);
    }

    public void removeStudent(Student student) {
        if (this.students == null) {
            return;
        }
        this.students.remove(student);
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    private void print() {
        System.out.println(this);
    }
}
