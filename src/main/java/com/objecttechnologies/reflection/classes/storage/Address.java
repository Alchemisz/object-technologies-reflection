package com.objecttechnologies.reflection.classes.storage;

import lombok.ToString;

@ToString
public class Address {

    private String streetName;
    private String city;

    public Address(String streetName, String city) {
        this.streetName = streetName;
        this.city = city;
    }
}
