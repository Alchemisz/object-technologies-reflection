package com.objecttechnologies.reflection.classes.storage;

import lombok.ToString;

@ToString
public class Person {

    private final Integer age;
    private Wallet wallet;

    public Person(Integer age) {
        this.age = age;
    }

    private void print() {
        System.out.println(this);
    }
}
