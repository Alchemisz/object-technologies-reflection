package com.objecttechnologies.reflection.classes.storage;

import lombok.ToString;

@ToString
public class Wallet {
    Integer amount;
}
