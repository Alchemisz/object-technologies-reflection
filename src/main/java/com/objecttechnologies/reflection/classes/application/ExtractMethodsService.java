package com.objecttechnologies.reflection.classes.application;

import com.objecttechnologies.reflection.classes.client.view.MethodView;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
class ExtractMethodsService {

    List<MethodView> extractMethods(String className) {
        var methodsDescriptions = new ArrayList<MethodView>();
        var methods = getDeclaredMethodsByClassName(className);

        for (Method method : methods) {
            var sb = new StringBuilder();

            appendModifier(sb, method.getModifiers());

            sb.append(method.getReturnType().getSimpleName())
                .append(" ")
                .append(method.getName())
                .append("(");

            var parameterTypes = method.getParameterTypes();
            var parameterTypesNames = extractParameterTypesNames(parameterTypes);
            appendParameters(sb, parameterTypes);

            sb.append(")");

            methodsDescriptions.add(
                MethodView.builder()
                    .value(sb.toString())
                    .name(method.getName())
                    .paramsTypesNames(parameterTypesNames)
                    .build()
            );
        }

        return methodsDescriptions;
    }

    private static Method[] getDeclaredMethodsByClassName(String className) {
        try {
            return Class.forName(className).getDeclaredMethods();
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    private static List<String> extractParameterTypesNames(Class<?>[] parameterTypes) {
        return Arrays.stream(parameterTypes)
            .map(Class::getName)
            .collect(Collectors.toList());
    }

    private static void appendModifier(StringBuilder sb, int modifier) {
        var modifiers = Modifier.toString(modifier);
        if (modifiers.length() > 0) {
            sb.append(modifiers).append(" ");
        }
    }

    private void appendParameters(StringBuilder sb, Class<?>[] paramTypes) {
        for (int j = 0; j < paramTypes.length; j++) {
            if (j > 0) {
                sb.append(", ");
            }
            sb.append(paramTypes[j].getSimpleName());
        }
    }

}
