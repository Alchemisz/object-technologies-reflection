package com.objecttechnologies.reflection.classes.application;

import com.objecttechnologies.reflection.classconfiguration.infrastructure.ClassConfigurationRepositoryPort;
import com.objecttechnologies.reflection.classes.client.view.ConstructorView;
import com.objecttechnologies.reflection.classes.client.view.MethodView;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ClassesQueryFacade {

    private final ClassConfigurationRepositoryPort classConfigurationRepositoryPort;
    private final ExtractContractorsService extractContractorsService;
    private final ExtractMethodsService extractMethodsService;

    public Set<String> getAvailableClasses() {
        return classConfigurationRepositoryPort.findActiveClassName();
    }

    public List<ConstructorView> findConstructors(String className) {
        return extractContractorsService.extractConstructors(className);
    }

    public List<MethodView> findMethods(String className) {
        return extractMethodsService.extractMethods(className);
    }
}
