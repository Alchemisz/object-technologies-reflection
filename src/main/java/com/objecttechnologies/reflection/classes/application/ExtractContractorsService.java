package com.objecttechnologies.reflection.classes.application;

import com.objecttechnologies.reflection.classes.client.view.ConstructorView;
import org.springframework.stereotype.Service;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
class ExtractContractorsService {

    List<ConstructorView> extractConstructors(String className) {
        var constructorDescriptions = new ArrayList<ConstructorView>();
        var constructors = getDeclaredConstructorsByClassName(className);

        for (Constructor<?> constructor : constructors) {
            var sb = new StringBuilder();

            appendModifier(sb, constructor.getModifiers());

            sb.append(className.split("[.]")[className.split("[.]").length - 1])
                .append("(");

            var parameterTypes = constructor.getParameterTypes();
            var parameterTypesNames = extractParameterTypesNames(parameterTypes);
            appendParameters(sb, parameterTypes);

            sb.append(")");

            constructorDescriptions.add(
                ConstructorView.builder()
                    .value(sb.toString())
                    .paramsTypesNames(parameterTypesNames)
                    .build()
            );
        }

        return constructorDescriptions;
    }

    private static List<String> extractParameterTypesNames(Class<?>[] parameterTypes) {
        return Arrays.stream(parameterTypes)
            .map(Class::getName)
            .collect(Collectors.toList());
    }

    private static Constructor<?>[] getDeclaredConstructorsByClassName(String className) {
        try {
            return Class.forName(className).getDeclaredConstructors();
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    private static void appendModifier(StringBuilder sb, int modifier) {
        var modifiers = Modifier.toString(modifier);
        if (modifiers.length() > 0) {
            sb.append(modifiers).append(" ");
        }
    }

    private void appendParameters(StringBuilder sb, Class<?>[] paramTypes) {
        for (int j = 0; j < paramTypes.length; j++) {
            if (j > 0) {
                sb.append(", ");
            }
            sb.append(paramTypes[j].getSimpleName());
        }
    }

}
