package com.objecttechnologies.reflection.classes.client.view;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class MethodView {
    String value;
    String name;
    List<String> paramsTypesNames;
}
