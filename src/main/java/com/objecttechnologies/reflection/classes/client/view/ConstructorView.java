package com.objecttechnologies.reflection.classes.client.view;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ConstructorView {
    String value;
    List<String> paramsTypesNames;
}
