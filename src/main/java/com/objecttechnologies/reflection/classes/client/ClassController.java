package com.objecttechnologies.reflection.classes.client;

import com.objecttechnologies.reflection.classes.application.ClassesQueryFacade;
import com.objecttechnologies.reflection.classes.client.view.ConstructorView;
import com.objecttechnologies.reflection.classes.client.view.MethodView;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/classes")
@RequiredArgsConstructor
public class ClassController {

    private final ClassesQueryFacade classesQueryFacade;

    @GetMapping("/available")
    public Set<String> getAvailableClasses() {
        return classesQueryFacade.getAvailableClasses();
    }

    @GetMapping("/{className}/constructors")
    public List<ConstructorView> getDeclaredConstructors(@PathVariable String className) {
        return classesQueryFacade.findConstructors(className);
    }

    @GetMapping("/{className}/methods")
    public List<MethodView> getDeclaredMethods(@PathVariable String className) {
        return classesQueryFacade.findMethods(className);
    }
}
