package com.objecttechnologies.reflection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ObjectTechnologiesReflectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(ObjectTechnologiesReflectionApplication.class, args);
	}

}
