package com.objecttechnologies.reflection.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {

    @Bean
    public Gson gson() {
        return new GsonBuilder()
            .serializeNulls()
            .setPrettyPrinting()
            .create();
    }

}
