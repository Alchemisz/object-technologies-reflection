package com.objecttechnologies.reflection.objectinstance.client.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ClassStructure {
    String className;
    List<String> fields;
    List<String> methods;
    List<String> constructors;
}
