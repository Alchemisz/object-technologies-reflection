package com.objecttechnologies.reflection.objectinstance.client.dto;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class AutocompleteViews {
    String className;
    List<AutocompleteView> views;

    @Value
    @Builder
    public static class AutocompleteView {
        String objectInstanceId;
        String toStringValue;
    }

}
