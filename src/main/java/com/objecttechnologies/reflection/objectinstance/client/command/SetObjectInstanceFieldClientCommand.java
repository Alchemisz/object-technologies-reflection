package com.objecttechnologies.reflection.objectinstance.client.command;

import lombok.Value;

@Value
public class SetObjectInstanceFieldClientCommand {
    String fieldName;
    Object value;
}
