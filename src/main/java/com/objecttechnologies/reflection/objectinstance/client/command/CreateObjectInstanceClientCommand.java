package com.objecttechnologies.reflection.objectinstance.client.command;

import lombok.Value;

@Value
public class CreateObjectInstanceClientCommand {
    String className;
    Object[] params;
}
