package com.objecttechnologies.reflection.objectinstance.client.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ObjectInstanceView {
    String id;
    String className;
    String stringRepresentation;
}
