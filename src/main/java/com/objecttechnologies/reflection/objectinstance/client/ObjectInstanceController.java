package com.objecttechnologies.reflection.objectinstance.client;

import com.objecttechnologies.reflection.objectinstance.application.ObjectInstanceFacade;
import com.objecttechnologies.reflection.objectinstance.client.command.CreateObjectInstanceClientCommand;
import com.objecttechnologies.reflection.objectinstance.client.command.InvokeObjectInstanceMethodClientCommand;
import com.objecttechnologies.reflection.objectinstance.client.command.SetObjectInstanceFieldClientCommand;
import com.objecttechnologies.reflection.objectinstance.client.dto.AutocompleteViews;
import com.objecttechnologies.reflection.objectinstance.client.dto.FieldView;
import com.objecttechnologies.reflection.objectinstance.client.dto.ObjectInstanceView;
import com.objecttechnologies.reflection.objectinstance.client.query.AutocompleteQuery;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceRepositoryPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/object-instances")
@RequiredArgsConstructor
public class ObjectInstanceController {

    private final ObjectInstanceRepositoryPort objectInstanceRepositoryPort;
    private final ObjectInstanceFacade objectInstanceFacade;

    @GetMapping
    public List<ObjectInstanceView> getAll() {
        return objectInstanceRepositoryPort.findAll();
    }

    @GetMapping("/{objectInstanceId}")
    public ObjectInstanceView getById(@PathVariable String objectInstanceId) {
        return objectInstanceRepositoryPort.findViewById(ObjectInstanceId.of(objectInstanceId));
    }

    @PostMapping("/create")
    public void createNewInstance(@RequestBody CreateObjectInstanceClientCommand command) {
        objectInstanceFacade.createNew(command);
    }

    @PatchMapping("/{objectInstanceId}/fields/set")
    public void setField(@PathVariable String objectInstanceId, @RequestBody SetObjectInstanceFieldClientCommand command) {
        objectInstanceFacade.setField(objectInstanceId, command);
    }

    @GetMapping("/{objectInstanceId}/fields/get")
    public List<FieldView> getFields(@PathVariable String objectInstanceId) {
        return objectInstanceFacade.findFields(ObjectInstanceId.of(objectInstanceId));
    }

    @PatchMapping("/{objectInstanceId}/methods/invoke")
    public void invokeMethod(@PathVariable String objectInstanceId, @RequestBody InvokeObjectInstanceMethodClientCommand command) {
        objectInstanceFacade.invokeMethod(objectInstanceId, command);
    }

    @PostMapping("/autocomplete")
    public List<AutocompleteViews> getAutocomplete(@RequestBody AutocompleteQuery query) {
        return objectInstanceRepositoryPort.getAutocomplete(query);
    }

    @ExceptionHandler(RuntimeException.class)
    public String handleException(Exception exception) {
        return exception.getMessage();
    }

}
