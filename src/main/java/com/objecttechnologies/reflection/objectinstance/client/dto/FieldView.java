package com.objecttechnologies.reflection.objectinstance.client.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class FieldView {
    String modifier;
    String simpleType;
    String type;
    String name;
    String value;
}
