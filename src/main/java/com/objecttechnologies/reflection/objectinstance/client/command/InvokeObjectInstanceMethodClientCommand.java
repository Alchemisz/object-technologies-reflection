package com.objecttechnologies.reflection.objectinstance.client.command;

import lombok.Value;

@Value
public class InvokeObjectInstanceMethodClientCommand {
    String methodName;
    Object[] params;
}
