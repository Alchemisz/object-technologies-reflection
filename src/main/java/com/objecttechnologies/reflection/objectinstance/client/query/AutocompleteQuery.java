package com.objecttechnologies.reflection.objectinstance.client.query;

import lombok.Data;

import java.util.List;

@Data
public class AutocompleteQuery {
    List<String> classNames;
}
