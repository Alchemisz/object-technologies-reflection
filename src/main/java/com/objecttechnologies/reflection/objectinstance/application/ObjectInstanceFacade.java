package com.objecttechnologies.reflection.objectinstance.application;

import com.objecttechnologies.reflection.objectinstance.client.command.CreateObjectInstanceClientCommand;
import com.objecttechnologies.reflection.objectinstance.client.command.InvokeObjectInstanceMethodClientCommand;
import com.objecttechnologies.reflection.objectinstance.client.command.SetObjectInstanceFieldClientCommand;
import com.objecttechnologies.reflection.objectinstance.client.dto.FieldView;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ObjectInstanceFacade {

    private final CreateObjectInstanceUseCase createObjectInstanceUseCase;
    private final SetObjectInstanceFieldUseCase setObjectInstanceFieldUseCase;
    private final InvokeObjectInstanceMethodUseCase invokeObjectInstanceMethodUseCase;
    private final ExtractFieldsService extractFieldsService;

    public void createNew(CreateObjectInstanceClientCommand command) {
        createObjectInstanceUseCase.create(command);
    }

    public void setField(String objectInstanceId, SetObjectInstanceFieldClientCommand command) {
        setObjectInstanceFieldUseCase.setField(ObjectInstanceId.of(objectInstanceId), command);
    }

    public void invokeMethod(String objectInstanceId, InvokeObjectInstanceMethodClientCommand command) {
        invokeObjectInstanceMethodUseCase.invokeMethod(ObjectInstanceId.of(objectInstanceId), command);
    }

    public List<FieldView> findFields(ObjectInstanceId objectInstanceId) {
        return extractFieldsService.extractFields(objectInstanceId);
    }
}
