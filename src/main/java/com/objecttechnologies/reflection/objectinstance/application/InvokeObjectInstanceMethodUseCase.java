package com.objecttechnologies.reflection.objectinstance.application;

import com.objecttechnologies.reflection.objectinstance.client.command.InvokeObjectInstanceMethodClientCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.InvokeObjectInstanceMethodCommand;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceCommandPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.objecttechnologies.reflection.shared.utils.AppUtils.isEmpty;

@Service
@RequiredArgsConstructor
class InvokeObjectInstanceMethodUseCase {

    private final ObjectInstanceCommandPort objectInstanceCommandPort;
    private final ObjectInstanceExtractorService objectInstanceExtractorService;

    void invokeMethod(ObjectInstanceId objectInstanceId, InvokeObjectInstanceMethodClientCommand command) {
        var createObjectInstanceCommand = buildCreateObjectInstanceCommand(objectInstanceId, command);
        objectInstanceCommandPort.invokeMethod(createObjectInstanceCommand);
    }

    private InvokeObjectInstanceMethodCommand buildCreateObjectInstanceCommand(ObjectInstanceId objectInstanceId, InvokeObjectInstanceMethodClientCommand command) {
        return InvokeObjectInstanceMethodCommand.builder()
            .objectInstanceId(objectInstanceId)
            .methodName(command.getMethodName())
            .params(prepareParameters(command.getParams()))
            .build();
    }

    private Object[] prepareParameters(Object[] parameters) {
        return !isEmpty(parameters)
            ? objectInstanceExtractorService.extractObjectInstance(parameters)
            : parameters;
    }

}
