package com.objecttechnologies.reflection.objectinstance.application;

import com.objecttechnologies.reflection.objectinstance.client.dto.FieldView;
import com.objecttechnologies.reflection.objectinstance.domain.ObjectInstance;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceRepositoryPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
class ExtractFieldsService {

    private final ObjectInstanceRepositoryPort objectInstanceRepositoryPort;

    List<FieldView> extractFields(ObjectInstanceId objectInstanceId) {
        ObjectInstance objectInstance = objectInstanceRepositoryPort.findById(objectInstanceId);

        var fieldViews = new ArrayList<FieldView>();
        var fields = objectInstance.getInstance().getClass().getDeclaredFields();

        for (Field field : fields) {
            var sb = new StringBuilder();

            appendModifier(sb, field.getModifiers());

            field.setAccessible(true);

            Object fieldValue = getFieldValue(objectInstance, field);
            fieldViews.add(
                FieldView.builder()
                    .modifier(sb.toString())
                    .type(field.getType().getName())
                    .simpleType(field.getType().getSimpleName())
                    .name(field.getName())
                    .value(fieldValue != null ? fieldValue.toString() : "null")
                    .build()
            );
        }

        return fieldViews;
    }

    private void appendModifier(StringBuilder sb, int modifier) {
        var modifiers = Modifier.toString(modifier);
        if (modifiers.length() > 0) {
            sb.append(modifiers);
        }
    }

    private Object getFieldValue(ObjectInstance objectInstance, Field field) {
        try {
            return field.get(objectInstance.getInstance());
        } catch (IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

}
