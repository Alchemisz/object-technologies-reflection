package com.objecttechnologies.reflection.objectinstance.application;

import com.objecttechnologies.reflection.objectinstance.client.command.SetObjectInstanceFieldClientCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.SetObjectInstanceFieldCommand;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceCommandPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
class SetObjectInstanceFieldUseCase {

    private final ObjectInstanceCommandPort objectInstanceCommandPort;
    private final ObjectInstanceExtractorService objectInstanceExtractorService;

    void setField(ObjectInstanceId objectInstanceId, SetObjectInstanceFieldClientCommand command) {
        SetObjectInstanceFieldCommand setObjectInstanceFieldCommand = SetObjectInstanceFieldCommand.builder()
            .objectInstanceId(objectInstanceId)
            .fieldName(command.getFieldName())
            .value(objectInstanceExtractorService.extractObjectInstance(List.of(command.getValue()).toArray())[0])
            .build();
        objectInstanceCommandPort.setField(setObjectInstanceFieldCommand);
    }
}
