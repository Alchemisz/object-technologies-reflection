package com.objecttechnologies.reflection.objectinstance.application;

import com.google.gson.Gson;
import com.objecttechnologies.reflection.classes.storage.Group;
import com.objecttechnologies.reflection.classes.storage.Student;
import com.objecttechnologies.reflection.objectinstance.domain.command.CreateObjectInstanceCommand;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceCommandPort;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceRepositoryPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
class ObjectInstanceExtractorService {

    private final ObjectInstanceRepositoryPort objectInstanceRepositoryPort;
    private final ObjectInstanceCommandPort objectInstanceCommandPort;
    private final Gson gson;
    private final Map<Class<?>, Set<String>> fieldsByClassName;

    ObjectInstanceExtractorService(ObjectInstanceRepositoryPort objectInstanceRepositoryPort, ObjectInstanceCommandPort objectInstanceCommandPort) {
        this.objectInstanceRepositoryPort = objectInstanceRepositoryPort;
        this.objectInstanceCommandPort = objectInstanceCommandPort;
        this.gson = new Gson();
        this.fieldsByClassName = prepareFieldsByClassName(Student.class, Group.class, ObjectInstanceId.class);
    }

    Object[] extractObjectInstance(Object[] objects) {
        for (int i = 0; i < objects.length; i++) {
            if (objects[i] instanceof Map<?, ?>) {
                objects[i] = tryExtractInstance((Map) objects[i]);
            } else {
                continue;
            }

            if (objects[i] instanceof ObjectInstanceId) {
                objects[i] = objectInstanceRepositoryPort.findById((ObjectInstanceId) objects[i]).getInstance();
                continue;
            }

            if (fieldsByClassName.containsKey(objects[i].getClass())) {
                createInstance(objects[i]);
            }

        }
        return objects;
    }

    private Object tryExtractInstance(Map<String, Object> potentialInstance) {
        Class<?> classType = getClassTypeIfExist(potentialInstance);

        if (classType == null) {
            return potentialInstance;
        }

        return gson.fromJson(potentialInstance.toString(), classType);
    }

    private Class<?> getClassTypeIfExist(Map<String, Object> potentialInstance) {
        return fieldsByClassName.entrySet().stream()
            .filter(entry -> entry.getValue().containsAll(potentialInstance.keySet()))
            .findFirst()
            .map(Map.Entry::getKey)
            .orElse(null);
    }

    private void createInstance(Object object) {
        objectInstanceCommandPort.create(
            CreateObjectInstanceCommand.builder()
                .className(object.getClass().getName())
                .params(extractFieldValues(object))
                .build()
        );
    }

    private static Object[] extractFieldValues(Object object) {
        return Arrays.stream(object.getClass().getDeclaredFields())
            .map(field -> {
                    field.setAccessible(true);
                    try {
                        return field.get(object);
                    } catch (IllegalAccessException e) {
                        throw new IllegalStateException(e);
                    }
                }
            ).toArray();
    }


    private Map<Class<?>, Set<String>> prepareFieldsByClassName(Class<?>... classTypes) {
        return Arrays.stream(classTypes)
            .collect(
                Collectors.toMap(
                    classType -> classType,
                    classType -> Arrays.stream(classType.getDeclaredFields()).map(Field::getName).collect(Collectors.toSet())
                )
            );
    }

}
