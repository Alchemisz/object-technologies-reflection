package com.objecttechnologies.reflection.objectinstance.application;

import com.objecttechnologies.reflection.objectinstance.client.command.CreateObjectInstanceClientCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.CreateObjectInstanceCommand;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceCommandPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static com.objecttechnologies.reflection.shared.utils.AppUtils.isEmpty;

@Service
@RequiredArgsConstructor
class CreateObjectInstanceUseCase {

    private final ObjectInstanceCommandPort objectInstanceCommandPort;
    private final ObjectInstanceExtractorService objectInstanceExtractorService;

    void create(CreateObjectInstanceClientCommand command) {
        var createObjectInstanceCommand = CreateObjectInstanceCommand.builder()
            .className(command.getClassName())
            .params(
                !isEmpty(command.getParams())
                    ? objectInstanceExtractorService.extractObjectInstance(command.getParams())
                    : command.getParams()
            )
            .build();
        objectInstanceCommandPort.create(createObjectInstanceCommand);
    }

}
