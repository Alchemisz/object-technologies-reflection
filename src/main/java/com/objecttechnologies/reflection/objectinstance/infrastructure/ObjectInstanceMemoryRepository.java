package com.objecttechnologies.reflection.objectinstance.infrastructure;

import com.objecttechnologies.reflection.objectinstance.domain.ObjectInstance;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
class ObjectInstanceMemoryRepository {
    private final Map<String, ObjectInstances> instancesByClassName;

    ObjectInstanceMemoryRepository() {
        this.instancesByClassName = new HashMap<>();
    }

    void persist(ObjectInstance objectInstance) {
        var objectInstances = instancesByClassName.get(objectInstance.getInstance().getClass().getName());

        if (objectInstances == null) {
            objectInstances = new ObjectInstances();
            this.instancesByClassName.put(objectInstance.getInstance().getClass().getName(), objectInstances);
        }

        objectInstances.insert(objectInstance);
    }

    Map<String, ObjectInstances> findAll() {
        return this.instancesByClassName;
    }

    List<ObjectInstances> findObjectInstancesByClassNameIncludesInheritance(String className) throws ClassNotFoundException {
        Class<?> classToSearch = Class.forName(className);
        List<Class<?>> allClasses = this.instancesByClassName.keySet().stream().map(name -> {
            try {
                return Class.forName(name);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList());

        return allClasses.stream()
            .filter(aClass -> aClass.isAssignableFrom(classToSearch) || classToSearch.isAssignableFrom(aClass))
            .map(Class::getName)
            .map(instancesByClassName::get)
            .collect(Collectors.toList());

//        if (!instancesByClassName.containsKey(className)) {
//            throw new IllegalArgumentException(String.format("Can not find instances for class name = %s", className));
//        }
//        return this.instancesByClassName.get(className);
    }

    Optional<ObjectInstance> findById(ObjectInstanceId objectInstanceId) {
        return instancesByClassName.values().stream()
            .flatMap(instancesById -> instancesById.getObjectInstances().stream())
            .filter(objectInstance -> objectInstance.getId().equals(objectInstanceId))
            .findFirst();
    }
}

@Value
class ObjectInstances {
    Map<ObjectInstanceId, ObjectInstance> objectInstanceById;

    ObjectInstances() {
        this.objectInstanceById = new HashMap<>();
    }

    void insert(ObjectInstance objectInstance) {
        this.objectInstanceById.put(objectInstance.getId(), objectInstance);
    }

    Collection<ObjectInstance> getObjectInstances() {
        return objectInstanceById.values();
    }
}