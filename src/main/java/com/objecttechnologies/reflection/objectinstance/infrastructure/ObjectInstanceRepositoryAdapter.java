package com.objecttechnologies.reflection.objectinstance.infrastructure;

import com.google.gson.Gson;
import com.objecttechnologies.reflection.classconfiguration.infrastructure.ClassConfigurationRepositoryPort;
import com.objecttechnologies.reflection.objectinstance.client.dto.AutocompleteViews;
import com.objecttechnologies.reflection.objectinstance.client.dto.ObjectInstanceView;
import com.objecttechnologies.reflection.objectinstance.client.query.AutocompleteQuery;
import com.objecttechnologies.reflection.objectinstance.domain.ObjectInstance;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceRepositoryPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class ObjectInstanceRepositoryAdapter implements ObjectInstanceRepositoryPort {

    private final ObjectInstanceMemoryRepository objectInstanceMemoryRepository;
    private final ClassConfigurationRepositoryPort classConfigurationRepositoryPort;
    private final Gson gson;


    @Override
    public List<ObjectInstanceView> findAll() {
        var objectInstancesByClassName = objectInstanceMemoryRepository.findAll();
        var activeClassName = classConfigurationRepositoryPort.findActiveClassName();

        return objectInstancesByClassName.entrySet().stream()
            .filter(entry -> activeClassName.contains(entry.getKey()))
            .flatMap(entry -> entry.getValue().getObjectInstances().stream())
            .map(objectInstance -> buildObjectInstanceView(objectInstance, objectInstance.getInstance().getClass().getName()))
            .collect(Collectors.toList());
    }

    @Override
    public ObjectInstance findById(ObjectInstanceId objectInstanceId) {
        return findByObjectInstanceId(objectInstanceId);
    }

    @Override
    public ObjectInstanceView findViewById(ObjectInstanceId objectInstanceId) {
        var objectInstance = findById(objectInstanceId);
        return buildObjectInstanceView(objectInstance, objectInstance.getInstance().getClass().getSimpleName());
    }

    @Override
    public void persist(ObjectInstance objectInstance) {
        objectInstanceMemoryRepository.persist(objectInstance);
    }

    @Override
    public List<AutocompleteViews> getAutocomplete(AutocompleteQuery query) {
        //TODO zrobic clean code pod spodem
        return query.getClassNames().stream()
            .map(this::buildViewsForClassName)
            .collect(Collectors.toList());
    }

    private ObjectInstanceView buildObjectInstanceView(ObjectInstance objectInstance, String className) {
        return ObjectInstanceView.builder()
            .id(objectInstance.getId().stringValue())
            .className(className)
            .stringRepresentation(gson.toJson(objectInstance.getInstance()))
            .build();
    }

    private AutocompleteViews buildViewsForClassName(String className) {
        var objectInstances = findObjectInstancesByClassNameIncludesInheritance(className);
        return AutocompleteViews.builder()
            .className(className)
            .views(buildObjectInstancesViews(objectInstances))
            .build();
    }

    private static List<AutocompleteViews.AutocompleteView> buildObjectInstancesViews(List<ObjectInstances> objectInstances) {
        return objectInstances.stream()
            .flatMap(instances -> instances.getObjectInstances().stream())
            .map(entry ->
                AutocompleteViews.AutocompleteView.builder()
                    .objectInstanceId(entry.getId().stringValue())
                    .toStringValue(entry.getInstance().toString())
                    .build()
            ).collect(Collectors.toList());
    }

    private List<ObjectInstances> findObjectInstancesByClassNameIncludesInheritance(String className) {
        try {
            return objectInstanceMemoryRepository.findObjectInstancesByClassNameIncludesInheritance(className);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private ObjectInstance findByObjectInstanceId(ObjectInstanceId objectInstanceId) {
        return objectInstanceMemoryRepository.findById(objectInstanceId)
            .orElseThrow(() -> new IllegalArgumentException(String.format("Can not find objectInstance ID = %s", objectInstanceId)));
    }
}
