package com.objecttechnologies.reflection.objectinstance.domain.command;

import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class InvokeObjectInstanceMethodCommand {
    ObjectInstanceId objectInstanceId;
    String methodName;
    Object[] params;
}
