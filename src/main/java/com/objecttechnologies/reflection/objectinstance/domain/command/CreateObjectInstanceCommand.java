package com.objecttechnologies.reflection.objectinstance.domain.command;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class CreateObjectInstanceCommand {
    String className;
    Object[] params;
}
