package com.objecttechnologies.reflection.objectinstance.domain;

import com.objecttechnologies.reflection.objectinstance.domain.command.CreateObjectInstanceCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.InvokeObjectInstanceMethodCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.SetObjectInstanceFieldCommand;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import lombok.Value;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.UUID;

import static com.objecttechnologies.reflection.shared.utils.AppUtils.isEmpty;

@Value
@RequiredArgsConstructor(staticName = "of")
public class ObjectInstance {
    ObjectInstanceId id;
    Object instance;

    static ObjectInstance of(CreateObjectInstanceCommand command) {
        return new ObjectInstance(
            ObjectInstanceId.of(UUID.randomUUID().toString()),
            tryCreateNewInstance(command.getClassName(), command.getParams())
        );
    }

    void setField(SetObjectInstanceFieldCommand command) {
        try {
            Field field = getFieldByName(command.getFieldName());
            field.setAccessible(true);
            field.set(instance, command.getValue());
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(
                String.format("Can not set field = %s, using value = %s, for class %s",
                    command.getFieldName(), command.getValue(), instance.getClass().getName())
            );
        }
    }

    void invokeMethod(InvokeObjectInstanceMethodCommand command) {
        try {
            Method method = getMethodByNameAndParamsTypes(command.getMethodName(), extractParamsTypes(command.getParams()));
            method.setAccessible(true);
            method.invoke(instance, command.getParams());
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new IllegalStateException(
                String.format("Can not invoke method = %s, using values = %s, for class %s",
                    command.getMethodName(), Arrays.toString(command.getParams()), instance.getClass().getName())
            );
        }
    }

    private static Object tryCreateNewInstance(String className, Object[] params) {
        try {
            Constructor<?> constructor = getConstructorByClassNameAndParamsTypes(className, extractParamsTypes(params));
            constructor.setAccessible(true);
            return constructor.newInstance(params);

        } catch (Exception ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    private static Class<?>[] extractParamsTypes(Object[] params) {
        if (isEmpty(params)) {
            return new Class[0];
        }
        return Arrays.stream(params)
            .map(Object::getClass)
            .toArray(Class[]::new);
    }

    private Field getFieldByName(String fieldName) {
        try {
            return instance.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException ex) {
            throw new IllegalStateException(String.format("Class %s does not contain field = %s", instance.getClass().getName(), fieldName));
        }
    }

    private Method getMethodByNameAndParamsTypes(String methodName, Class<?>[] paramsTypes) {
        try {
            return instance.getClass().getDeclaredMethod(methodName, paramsTypes);
        } catch (NoSuchMethodException ex) {
            return retryGetMethodByNameAndParamsTypes(methodName, Arrays.stream(paramsTypes).map(parameter -> Object.class).toArray(Class[]::new));
        }
    }

    //TODO to zostało zrobione specjalnie dla kolekcji zmiast tego trzeba zrobić dziedziczenie
    private Method retryGetMethodByNameAndParamsTypes(String methodName, Class<?>[] paramsTypes) {
        try {
            return instance.getClass().getDeclaredMethod(methodName, paramsTypes);
        } catch (NoSuchMethodException ex) {
            throw new IllegalStateException(String.format("Class %s does not contain method = %s for parameters = %s",
                instance.getClass().getName(), methodName, Arrays.toString(paramsTypes)));
        }
    }

    private static Constructor<?> getConstructorByClassNameAndParamsTypes(String className, Class<?>[] paramsTypes) {
        try {
            return Class.forName(className).getDeclaredConstructor(paramsTypes);
        } catch (NoSuchMethodException ex) {
            throw new IllegalStateException(
                String.format("Class %s does not contain constructor for parameters = %s", className, Arrays.toString(paramsTypes))
            );
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(String.format("Class %s does not exist", className));
        }
    }

}
