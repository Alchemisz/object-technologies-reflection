package com.objecttechnologies.reflection.objectinstance.domain.port;

import com.objecttechnologies.reflection.objectinstance.domain.command.CreateObjectInstanceCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.InvokeObjectInstanceMethodCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.SetObjectInstanceFieldCommand;

public interface ObjectInstanceCommandPort {
    void setField(SetObjectInstanceFieldCommand setObjectInstanceFieldCommand);

    void create(CreateObjectInstanceCommand command);

    void invokeMethod(InvokeObjectInstanceMethodCommand createObjectInstanceCommand);
}
