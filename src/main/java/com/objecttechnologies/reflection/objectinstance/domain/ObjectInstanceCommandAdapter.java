package com.objecttechnologies.reflection.objectinstance.domain;

import com.objecttechnologies.reflection.objectinstance.domain.command.CreateObjectInstanceCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.InvokeObjectInstanceMethodCommand;
import com.objecttechnologies.reflection.objectinstance.domain.command.SetObjectInstanceFieldCommand;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceCommandPort;
import com.objecttechnologies.reflection.objectinstance.domain.port.ObjectInstanceRepositoryPort;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
class ObjectInstanceCommandAdapter implements ObjectInstanceCommandPort {

    private final ObjectInstanceRepositoryPort objectInstanceRepositoryPort;

    @Override
    public void setField(SetObjectInstanceFieldCommand command) {
        var objectInstance = findById(command.getObjectInstanceId());
        objectInstance.setField(command);
        update(objectInstance);
    }

    @Override
    public void create(CreateObjectInstanceCommand command) {
        var objectInstance = ObjectInstance.of(command);
        update(objectInstance);
    }

    @Override
    public void invokeMethod(InvokeObjectInstanceMethodCommand command) {
        var objectInstance = findById(command.getObjectInstanceId());
        objectInstance.invokeMethod(command);
        update(objectInstance);
    }

    private ObjectInstance findById(ObjectInstanceId command) {
        return objectInstanceRepositoryPort.findById(command);
    }

    private void update(ObjectInstance objectInstance) {
        objectInstanceRepositoryPort.persist(objectInstance);
    }

}
