package com.objecttechnologies.reflection.objectinstance.domain.port;

import com.objecttechnologies.reflection.objectinstance.client.dto.AutocompleteViews;
import com.objecttechnologies.reflection.objectinstance.client.query.AutocompleteQuery;
import com.objecttechnologies.reflection.objectinstance.domain.ObjectInstance;
import com.objecttechnologies.reflection.objectinstance.client.dto.ObjectInstanceView;
import com.objecttechnologies.reflection.shared.vo.ObjectInstanceId;

import java.util.List;

public interface ObjectInstanceRepositoryPort {
    List<ObjectInstanceView> findAll();

    ObjectInstance findById(ObjectInstanceId objectInstanceId);

    ObjectInstanceView findViewById(ObjectInstanceId objectInstanceId);

    void persist(ObjectInstance objectInstance);

    List<AutocompleteViews> getAutocomplete(AutocompleteQuery query);
}
